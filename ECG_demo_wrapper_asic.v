`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/07/21 20:53:26
// Design Name: 
// Module Name: ECG_demo_wrapper_asic
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ECG_demo_wrapper_asic(
    input clk_sys_n,clk_sys_p,
    input rst_n,

    input rx,
    input [3:0] config_set,
    input en,

    output [3:0] bit_code,
    output [7:0] duan_code,
    output tx,

    output finish,
    output initial_ok,

    // input lcd_rx,
    // output lcd_tx,

    output  [2:0]   OpCode,
    output          nn_en,
    output          nn_ram_we,
    output  [11:0]  nn_ram_addr,
    output  [15:0]  nn_ram_din,
    output  [7:0]   Monitor_opcode,
    output  [1:0]   chip_en,
    output          clk_chip,
    input nn_finish,
    input [4:0] result,
    input [15:0] Check_out


    );
assign clk_chip = ~clk_sys;


parameter T = 11;
parameter t = 5000;

assign chip_en = 2'b11;    //ECG_HNN model


clk_wiz_0 u_clk_gen_top
 (
  // Clock out ports
  .clk(clk),
  .wr_clk(wr_clk),
 // Clock in ports
  .clk_in1_p(clk_sys_p),
  .clk_in1_p(clk_sys_n)
 );

wire [7:0] uart_out;
wire uart_ready;
wire full;
wire wr_en;
wire [95:0] din;
wire [7:0] ready_top;


TOPECGEncoder u_LC_ADC(
    .clk(wr_clk), 
    .rst_n(rst_n),
    
    .UartData_top(uart_out) ,
	.Valid_top(uart_ready), // uart receiver data[7:0] is valid
	.Full_top(full), // FIFO interface for core logic
	
	.WrtFIFOEn_top(wr_en),
	.EncodedFrame_top(din),
	
	.ready_top(ready_top)
    );


uart_rx #(
    87, // frequency divided by baud rate 
    8
)  u_rx  (
    .clk(wr_clk),
    .rst_n(rst_n),
    .rx(rx),

    .uart_ready(uart_ready),   //串转�?
    .uart_out(uart_out)
    );


wire totx_ready;
wire [7:0] data2tx;
wire tx_ready;

uart_tx #(43) u_tx (
    .clk(clk),
    .rstn(rst_n),
    .data_ready(totx_ready),      //inner 2 uart
    .data_in(data2tx),
    .tx_ready(tx_ready),
    .txd(tx)
    );

wire fifo_empty;
wire [95:0] dout;
wire rd_en;

fifo_generator_0 u_fifo (
    .wr_clk(wr_clk),
    .rd_clk(clk),

    .din(din),
    .wr_en(wr_en),
    .full(full),
    .empty(fifo_empty),
    .dout(dout),
    .rd_en(rd_en)

);

// wire [15:0] Check_out;
wire [11:0] beat;


Processor_top_wrapper_for_asic #(T) u_nn_wrapper_asic
(
    .clk(clk),
    .rst_n(rst_n),
    .en(en),
    .fifo_empty(fifo_empty),
    // .drop_out(4'd0),
    .config_set(config_set),
    .dout(dout),
    .rd_en(rd_en),
    .finish(finish),
    .result(result),
    .data2tx(data2tx),
    .totx_ready(totx_ready),
    .Check_out(Check_out),
    .initial_ok(initial_ok),
    .beat(beat),

    .OpCode(OpCode),
    .nn_en(nn_en),
    .nn_ram_we(nn_ram_we),
    .nn_ram_addr(nn_ram_addr),
    .nn_ram_din(nn_ram_din),
    .Monitor_opcode(Monitor_opcode),
    .nn_finish(nn_finish)
  
    );

// Processor_top  u_Processor_top (
//     .clk                     ( clk                    ),
//     .rst_nasy                ( rst_n                  ),
//     .OpCode                  ( OpCode          [2:0]  ),
//     .EN                      ( nn_en                     ),
//     .RAM_WE                  ( nn_ram_we                 ),
//     .RAM_ADDR                ( nn_ram_addr        [11:0] ),
//     .RAM_DIN                 ( nn_ram_din         [15:0] ),
//     .Monitor_opcode          ( Monitor_opcode  [3:0]  ),

//     .Finish                  ( nn_finish                 ),
//     .Result                  ( result          [4:0]  ),
//     //.RAM_DOUT                ( RAM_DOUT        [15:0] ),
//     .Check_out               ( Check_out       [15:0] )
// );

beat_display #(t) u_beat_disp(
    .clk(clk),
    .rst_n(rst_n),
    .beat(beat),
    .bit_code(bit_code),
    .duan_code(duan_code)
    ); 


ila_0 u_ila_0(
.clk(clk_sys),


.probe0(din),
.probe1(wr_en),
.probe2(Check_out),
.probe3(result)
);


// wire lcd_tx;
// wire lcd_rx;

 


// lcd_top #(
//     8,
//     8               
//     ) (
//     .clk(clk)                     ,           // ϵͳʱ��
//     .rst_n(rst_n)                   ,           // ȫ�ָ�λ�źţ�
    
//     // uart
//     .rx(lcd_rx)                      ,           // ��������˿ڣ�?
//     .tx(lcd_tx)                      ,           // ��������˿ڣ�?
    
//     // data to LCD
//     .mse(uart_out)                      ,
//     .mse_valid(uart_ready)                ,
    
//     .kws_class(result)               ,
//     .kws_class_valid(finish)         ,
    
//     .sv_class(8'd0)                ,
//     .sv_class_valid(1'b0)          
// );












endmodule
