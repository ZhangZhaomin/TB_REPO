#################FPGA##################
    input clk_sys,








set_property -dict {PACKAGE_PIN AB16 IOSTANDARD LVCMOS18} [get_ports rst_n}

set_property -dict {PACKAGE_PIN AD30 IOSTANDARD LVCMOS33} [get_ports rx}

set_property -dict {PACKAGE_PIN AB17 IOSTANDARD LVCMOS18} [get_ports config_set[0]}
set_property -dict {PACKAGE_PIN AK15 IOSTANDARD LVCMOS18} [get_ports config_set[1]}
set_property -dict {PACKAGE_PIN AJ15 IOSTANDARD LVCMOS18} [get_ports config_set[2]}
set_property -dict {PACKAGE_PIN AC17 IOSTANDARD LVCMOS18} [get_ports config_set[3]}

set_property -dict {PACKAGE_PIN AC16 IOSTANDARD LVCMOS18} [get_ports en}

set_property -dict {PACKAGE_PIN AJ14 IOSTANDARD LVCMOS18} [get_ports bit_code[0]}
set_property -dict {PACKAGE_PIN AJ13 IOSTANDARD LVCMOS18} [get_ports bit_code[1]}
set_property -dict {PACKAGE_PIN AF13 IOSTANDARD LVCMOS18} [get_ports bit_code[2]}
set_property -dict {PACKAGE_PIN AE13 IOSTANDARD LVCMOS18} [get_ports bit_code[3]}

set_property -dict {PACKAGE_PIN AE30 IOSTANDARD LVCMOS33} [get_ports tx}

set_property -dict {PACKAGE_PIN AD14 IOSTANDARD LVCMOS18} [get_ports finish}

set_property -dict {PACKAGE_PIN AD13 IOSTANDARD LVCMOS18} [get_ports initial_ok}

##############CHIP#############





    output reg          clk_chip,
    input nn_finish,
    input [4:0] result,


set_property -dict {PACKAGE_PIN G12 IOSTANDARD LVCMOS18} [get_ports OpCode[0]}
set_property -dict {PACKAGE_PIN F12 IOSTANDARD LVCMOS18} [get_ports OpCode[1]}
set_property -dict {PACKAGE_PIN B11 IOSTANDARD LVCMOS18} [get_ports OpCode[2]}

set_property -dict {PACKAGE_PIN H13 IOSTANDARD LVCMOS18} [get_ports nn_en}

set_property -dict {PACKAGE_PIN J13 IOSTANDARD LVCMOS18} [get_ports nn_ram_we}

set_property -dict {PACKAGE_PIN L15 IOSTANDARD LVCMOS18} [get_ports nn_ram_addr[0]}
set_property -dict {PACKAGE_PIN A17 IOSTANDARD LVCMOS18} [get_ports nn_ram_addr[1]}
set_property -dict {PACKAGE_PIN E17 IOSTANDARD LVCMOS18} [get_ports nn_ram_addr[2]}
set_property -dict {PACKAGE_PIN L14 IOSTANDARD LVCMOS18} [get_ports nn_ram_addr[3]}
set_property -dict {PACKAGE_PIN B17 IOSTANDARD LVCMOS18} [get_ports nn_ram_addr[4]}
set_property -dict {PACKAGE_PIN G16 IOSTANDARD LVCMOS18} [get_ports nn_ram_addr[5]}
set_property -dict {PACKAGE_PIN C16 IOSTANDARD LVCMOS18} [get_ports nn_ram_addr[6]}
set_property -dict {PACKAGE_PIN F17 IOSTANDARD LVCMOS18} [get_ports nn_ram_addr[7]}
set_property -dict {PACKAGE_PIN G17 IOSTANDARD LVCMOS18} [get_ports nn_ram_addr[8]}
set_property -dict {PACKAGE_PIN D16 IOSTANDARD LVCMOS18} [get_ports nn_ram_addr[9]}
set_property -dict {PACKAGE_PIN H16 IOSTANDARD LVCMOS18} [get_ports nn_ram_addr[10]}
set_property -dict {PACKAGE_PIN J16 IOSTANDARD LVCMOS18} [get_ports nn_ram_addr[11]}

set_property -dict {PACKAGE_PIN A15 IOSTANDARD LVCMOS18} [get_ports nn_ram_din[15]}
set_property -dict {PACKAGE_PIN B15 IOSTANDARD LVCMOS18} [get_ports nn_ram_din[14]}
set_property -dict {PACKAGE_PIN A14 IOSTANDARD LVCMOS18} [get_ports nn_ram_din[13]}
set_property -dict {PACKAGE_PIN B14 IOSTANDARD LVCMOS18} [get_ports nn_ram_din[12]}
set_property -dict {PACKAGE_PIN E13 IOSTANDARD LVCMOS18} [get_ports nn_ram_din[11]}
set_property -dict {PACKAGE_PIN D13 IOSTANDARD LVCMOS18} [get_ports nn_ram_din[10]}
set_property -dict {PACKAGE_PIN F15 IOSTANDARD LVCMOS18} [get_ports nn_ram_din[9]}
set_property -dict {PACKAGE_PIN F14 IOSTANDARD LVCMOS18} [get_ports nn_ram_din[8]}
set_property -dict {PACKAGE_PIN E12 IOSTANDARD LVCMOS18} [get_ports nn_ram_din[7]}
set_property -dict {PACKAGE_PIN F13 IOSTANDARD LVCMOS18} [get_ports nn_ram_din[6]}
set_property -dict {PACKAGE_PIN J15 IOSTANDARD LVCMOS18} [get_ports nn_ram_din[5]}
set_property -dict {PACKAGE_PIN K15 IOSTANDARD LVCMOS18} [get_ports nn_ram_din[4]}
set_property -dict {PACKAGE_PIN L13 IOSTANDARD LVCMOS18} [get_ports nn_ram_din[3]}
set_property -dict {PACKAGE_PIN K13 IOSTANDARD LVCMOS18} [get_ports nn_ram_din[2]}
set_property -dict {PACKAGE_PIN G15 IOSTANDARD LVCMOS18} [get_ports nn_ram_din[1]}
set_property -dict {PACKAGE_PIN G14 IOSTANDARD LVCMOS18} [get_ports nn_ram_din[0]}

set_property -dict {PACKAGE_PIN Y21 IOSTANDARD LVCMOS18} [get_ports Monitor_opcode[3]}
set_property -dict {PACKAGE_PIN AF19 IOSTANDARD LVCMOS18} [get_ports Monitor_opcode[2]}
set_property -dict {PACKAGE_PIN AG19 IOSTANDARD LVCMOS18} [get_ports Monitor_opcode[1]}
set_property -dict {PACKAGE_PIN AH19 IOSTANDARD LVCMOS18} [get_ports Monitor_opcode[0]}



set_property -dict {PACKAGE_PIN AH13 IOSTANDARD LVCMOS18} [get_ports chip_en[1]}
set_property -dict {PACKAGE_PIN AH14 IOSTANDARD LVCMOS18} [get_ports chip_en[0]}

set_property -dict {PACKAGE_PIN AH13 AE15 IOSTANDARD LVCMOS18} [get_ports Check_out[0]}
set_property -dict {PACKAGE_PIN AH13 AE16 IOSTANDARD LVCMOS18} [get_ports Check_out[1]}
set_property -dict {PACKAGE_PIN AH13 AD16 IOSTANDARD LVCMOS18} [get_ports Check_out[10]}
set_property -dict {PACKAGE_PIN AH13 AD15 IOSTANDARD LVCMOS18} [get_ports Check_out[11]}
set_property -dict {PACKAGE_PIN AH13 AG15 IOSTANDARD LVCMOS18} [get_ports Check_out[12]}
set_property -dict {PACKAGE_PIN AH13 AF15 IOSTANDARD LVCMOS18} [get_ports Check_out[13]}
set_property -dict {PACKAGE_PIN AH13 AK13 IOSTANDARD LVCMOS18} [get_ports Check_out[14]}
set_property -dict {PACKAGE_PIN AH13 AK12 IOSTANDARD LVCMOS18} [get_ports Check_out[15]}
set_property -dict {PACKAGE_PIN AH13 AJ18 IOSTANDARD LVCMOS18} [get_ports Check_out[2]}
set_property -dict {PACKAGE_PIN AH13 AJ16 IOSTANDARD LVCMOS18} [get_ports Check_out[3]}
set_property -dict {PACKAGE_PIN AH13 AK16 IOSTANDARD LVCMOS18} [get_ports Check_out[4]}
set_property -dict {PACKAGE_PIN AH13 AG16 IOSTANDARD LVCMOS18} [get_ports Check_out[5]}
set_property -dict {PACKAGE_PIN AH13 AG17 IOSTANDARD LVCMOS18} [get_ports Check_out[6]}
set_property -dict {PACKAGE_PIN AH13 AH18 IOSTANDARD LVCMOS18} [get_ports Check_out[7]}
set_property -dict {PACKAGE_PIN AH13 AA15 IOSTANDARD LVCMOS18} [get_ports Check_out[8]}
set_property -dict {PACKAGE_PIN AH13 AA14 IOSTANDARD LVCMOS18} [get_ports Check_out[9]}









