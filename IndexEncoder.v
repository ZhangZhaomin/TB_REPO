`timescale	1ns/1ps

`define UseSkipping 1'b0 // change the compile code snippet

module 		IndexEncoder(
		input	clk, rst_n,
		input				StartIdxEncode,
		input	[95:0]		EncodedFrame,
		// input				Empty,

		output	reg	[15:0]		IdxValue,
		output	reg [11:0]		Addr_RF, // 0,...
		output	reg			WrtEn_RF, // 3 clks, mimic the latch timing constrains
		// output	reg			RdEn_FIFO,
		output	reg			FinishIdxEncode
	);

`ifdef UseSkipping
// ==== state parameters ====
localparam	S_Idle = 					4'd0,
			S_Fetch_Frame_LoadData=		4'd2,
			S_Encoding_Judgebit = 		4'd4,
			S_Encoding_WriteIndex1 = 	4'd5,
			S_Encoding_WriteIndex2 = 	4'd6,
			S_Encoding_WriteIndex3 = 	4'd7,
			S_WriteTail1		= 		4'd9,
			S_WriteTail2		= 		4'd10,
			S_WriteTail3		= 		4'd11, 
			S_Finish = 					4'd12;

// ==== state transfer ====

// internal regs
reg		[95:0]		encoded_frame_reg; 				// store the encoded frame read from FIFO
reg 	[6:0]		cnt_num_nonzero_bit;			// indicate which bit to be encoded
reg 	[6:0]		cnt_num_bit;					// calculate the num. of processed bits

// state regs
reg		[3:0]	state, next_state;

always @(posedge clk or negedge rst_n) begin
	if (!rst_n) begin
		state <= S_Idle;
	end
	else begin
		state <= next_state;
	end
end

always@(*)begin
	case(state)
	S_Idle:begin
		next_state = StartIdxEncode? S_Fetch_Frame_LoadData : S_Idle;
	end
	S_Fetch_Frame_LoadData:begin
		next_state = S_Encoding_Judgebit;
	end
	S_Encoding_Judgebit:begin
		next_state = cnt_num_bit == 96 ? S_WriteTail1 : 
					 encoded_frame_reg[95-cnt_num_bit] == 1 ? S_Encoding_WriteIndex1 : S_Encoding_Judgebit;
	end
	S_Encoding_WriteIndex1:begin
		next_state = S_Encoding_WriteIndex2;
	end
	S_Encoding_WriteIndex2:begin
		next_state = S_Encoding_WriteIndex3;
	end
	S_Encoding_WriteIndex3:begin
		next_state = S_Encoding_Judgebit;
	end
	S_WriteTail1:begin
		next_state = S_WriteTail2;
	end
	S_WriteTail2:begin
		next_state = S_WriteTail3;
	end
	S_WriteTail3:begin
		next_state = S_Finish;
	end
	S_Finish:begin
		next_state = S_Idle;
	end
	default: begin 
		next_state = S_Idle;
	end
	endcase
end

// ==== internal regs computing ====
always @(posedge clk or negedge rst_n) begin
	if (!rst_n) begin
		encoded_frame_reg <= 0; 			
		cnt_num_nonzero_bit <= 0;			
		cnt_num_bit <= 0;			
	end
	else begin
		case(state)
		S_Idle:begin
			encoded_frame_reg <= 0; 			
			cnt_num_nonzero_bit <= 0;			
			cnt_num_bit <= 0;
		end
		S_Fetch_Frame_LoadData:begin
			encoded_frame_reg <= EncodedFrame; 			
			cnt_num_nonzero_bit <= 0;			
			cnt_num_bit <= 0;
		end
		S_Encoding_Judgebit:begin
			encoded_frame_reg <= encoded_frame_reg; 			
			cnt_num_nonzero_bit <= cnt_num_nonzero_bit;			
			cnt_num_bit <= cnt_num_bit + 1;
		end
		S_Encoding_WriteIndex1:begin
			encoded_frame_reg <= encoded_frame_reg; 			
			cnt_num_nonzero_bit <= cnt_num_nonzero_bit;			
			cnt_num_bit <= cnt_num_bit;
		end
		S_Encoding_WriteIndex2:begin
			encoded_frame_reg <= encoded_frame_reg; 			
			cnt_num_nonzero_bit <= cnt_num_nonzero_bit;			
			cnt_num_bit <= cnt_num_bit;
		end
		S_Encoding_WriteIndex3:begin
			encoded_frame_reg <= encoded_frame_reg; 			
			cnt_num_nonzero_bit <= cnt_num_nonzero_bit + 1;			
			cnt_num_bit <= cnt_num_bit;
		end
		S_WriteTail1:begin
			encoded_frame_reg <= encoded_frame_reg; 			
			cnt_num_nonzero_bit <= cnt_num_nonzero_bit;			
			cnt_num_bit <= cnt_num_bit;
		end
		S_WriteTail2:begin
			encoded_frame_reg <= encoded_frame_reg; 			
			cnt_num_nonzero_bit <= cnt_num_nonzero_bit;			
			cnt_num_bit <= cnt_num_bit;
		end
		S_WriteTail3:begin
			encoded_frame_reg <= encoded_frame_reg; 			
			cnt_num_nonzero_bit <= cnt_num_nonzero_bit;			
			cnt_num_bit <= cnt_num_bit;
		end
		S_Finish:begin
			encoded_frame_reg <= encoded_frame_reg; 			
			cnt_num_nonzero_bit <= cnt_num_nonzero_bit;			
			cnt_num_bit <= cnt_num_bit;
		end
		default: begin 
			encoded_frame_reg <= 0; 			
			cnt_num_nonzero_bit <= 0;			
			cnt_num_bit <= 0;

		end
		endcase
	end
end


// ==== output regs computing ====
always @(posedge clk or negedge rst_n) begin
	if (!rst_n) begin
		IdxValue <= 0;
		Addr_RF <= 0;
		WrtEn_RF <= 0;
		FinishIdxEncode	<= 0;			
	end
	else begin
		case(state)
		S_Idle:begin
			IdxValue <= 0;
			Addr_RF <= 0;
			WrtEn_RF <= 0;
			FinishIdxEncode	<= 0;
		end
		S_Fetch_Frame_LoadData:begin
			IdxValue <= 0;
			Addr_RF <= 0;
			WrtEn_RF <= 0;
			FinishIdxEncode	<= 0;
		end
		S_Encoding_Judgebit:begin
			IdxValue <= 0;
			Addr_RF <= 0;
			WrtEn_RF <= 0;
			FinishIdxEncode	<= 0;
		end
		S_Encoding_WriteIndex1:begin
			IdxValue <= {9'b0, cnt_num_bit-1};
			Addr_RF <= 12'd3840 + {5'b0, cnt_num_nonzero_bit};
			WrtEn_RF <= 0;
			FinishIdxEncode	<= 0;
		end
		S_Encoding_WriteIndex2:begin
			IdxValue <= {9'b0, cnt_num_bit-1};
			Addr_RF <= 12'd3840 + {5'b0, cnt_num_nonzero_bit};
			WrtEn_RF <= 1;
			FinishIdxEncode	<= 0;
		end
		S_Encoding_WriteIndex3:begin
			IdxValue <= {9'b0, cnt_num_bit-1};
			Addr_RF <= 12'd3840 + {5'b0, cnt_num_nonzero_bit};
			WrtEn_RF <= 0;
			FinishIdxEncode	<= 0;
		end
		S_WriteTail1:begin
			IdxValue <= 16'b0000_0000_01111111;
			Addr_RF <= 12'd3840 + {5'b0, cnt_num_nonzero_bit};
			WrtEn_RF <= 0;
			FinishIdxEncode	<= 0;
		end
		S_WriteTail2:begin
			IdxValue <= 16'b0000_0000_01111111;
			Addr_RF <= 12'd3840 + {5'b0, cnt_num_nonzero_bit};
			WrtEn_RF <= 1;
			FinishIdxEncode	<= 0;
		end
		S_WriteTail3:begin
			IdxValue <= 16'b0000_0000_01111111;
			Addr_RF <= 12'd3840 + {5'b0, cnt_num_nonzero_bit};
			WrtEn_RF <= 0;
			FinishIdxEncode	<= 0;
		end
		S_Finish:begin
			IdxValue <= 0;
			Addr_RF <= 0;
			WrtEn_RF <= 0;
			FinishIdxEncode	<= 1;
		end
		default: begin 
			IdxValue <= 0;
			Addr_RF <= 0;
			WrtEn_RF <= 0;
			FinishIdxEncode	<= 0;
		end
		endcase
	end
end

`else //====================================================================================================================================================================
// ==== state parameters ====
localparam	S_Idle = 					4'd0,
			S_Fetch_Frame_LoadData=		4'd2,
			S_Encoding_Judgebit = 		4'd4,
			S_Encoding_WriteIndex1 = 	4'd5,
			S_Encoding_WriteIndex2 = 	4'd6,
			S_Encoding_WriteIndex3 = 	4'd7,
			S_Finish = 					4'd12;

// ==== state transfer ====

// internal regs
reg		[95:0]		encoded_frame_reg; 				// store the encoded frame read from FIFO
reg 	[6:0]		cnt_num_bit;					// calculate the num. of processed bits

// state regs
reg		[3:0]	state, next_state;

always @(posedge clk or negedge rst_n) begin
	if (!rst_n) begin
		state <= S_Idle;
	end
	else begin
		state <= next_state;
	end
end

always@(*)begin
	case(state)
	S_Idle:begin
		next_state = StartIdxEncode? S_Fetch_Frame_LoadData : S_Idle;
	end
	S_Fetch_Frame_LoadData:begin
		next_state = S_Encoding_Judgebit;
	end
	S_Encoding_Judgebit:begin
		next_state = cnt_num_bit == 96 ? S_Finish : S_Encoding_WriteIndex1;
	end
	S_Encoding_WriteIndex1:begin
		next_state = S_Encoding_WriteIndex2;
	end
	S_Encoding_WriteIndex2:begin
		next_state = S_Encoding_WriteIndex3;
	end
	S_Encoding_WriteIndex3:begin
		next_state = S_Encoding_Judgebit;
	end
	S_Finish:begin
		next_state = S_Idle;
	end
	default: begin 
		next_state = S_Idle;
	end
	endcase
end

// ==== internal regs computing ====
always @(posedge clk or negedge rst_n) begin
	if (!rst_n) begin
		encoded_frame_reg <= 0; 					
		cnt_num_bit <= 0;			
	end
	else begin
		case(state)
		S_Idle:begin
			encoded_frame_reg <= 0; 					
			cnt_num_bit <= 0;
		end
		S_Fetch_Frame_LoadData:begin
			encoded_frame_reg <= EncodedFrame; 					
			cnt_num_bit <= 0;
		end
		S_Encoding_Judgebit:begin
			encoded_frame_reg <= encoded_frame_reg; 					
			cnt_num_bit <= cnt_num_bit + 1;
		end
		S_Encoding_WriteIndex1:begin
			encoded_frame_reg <= encoded_frame_reg; 					
			cnt_num_bit <= cnt_num_bit;
		end
		S_Encoding_WriteIndex2:begin
			encoded_frame_reg <= encoded_frame_reg; 					
			cnt_num_bit <= cnt_num_bit;
		end
		S_Encoding_WriteIndex3:begin
			encoded_frame_reg <= encoded_frame_reg; 					
			cnt_num_bit <= cnt_num_bit;
		end
		S_Finish:begin
			encoded_frame_reg <= encoded_frame_reg; 					
			cnt_num_bit <= cnt_num_bit;
		end
		default: begin 
			encoded_frame_reg <= 0; 					
			cnt_num_bit <= 0;

		end
		endcase
	end
end


// ==== output regs computing ====
wire  [6:0] wire_cnt_num_bit_minus_one = cnt_num_bit-1;
always @(posedge clk or negedge rst_n) begin
	if (!rst_n) begin
		IdxValue <= 0;
		Addr_RF <= 0;
		WrtEn_RF <= 0;
		FinishIdxEncode	<= 0;			
	end
	else begin
		case(state)
		S_Idle:begin
			IdxValue <= 0;
			Addr_RF <= 0;
			WrtEn_RF <= 0;
			FinishIdxEncode	<= 0;
		end
		S_Fetch_Frame_LoadData:begin
			IdxValue <= 0;
			Addr_RF <= 0;
			WrtEn_RF <= 0;
			FinishIdxEncode	<= 0;
		end
		S_Encoding_Judgebit:begin
			IdxValue <= 0;
			Addr_RF <= 0;
			WrtEn_RF <= 0;
			FinishIdxEncode	<= 0;
		end
		S_Encoding_WriteIndex1:begin
			IdxValue <= {15'b0, encoded_frame_reg[wire_cnt_num_bit_minus_one]};
			Addr_RF <= 12'd3840 + {5'b0, wire_cnt_num_bit_minus_one};
			WrtEn_RF <= 0;
			FinishIdxEncode	<= 0;
		end
		S_Encoding_WriteIndex2:begin
			IdxValue <= {15'b0, encoded_frame_reg[wire_cnt_num_bit_minus_one]};
			Addr_RF <= 12'd3840 + {5'b0, wire_cnt_num_bit_minus_one};
			WrtEn_RF <= 1;
			FinishIdxEncode	<= 0;
		end
		S_Encoding_WriteIndex3:begin
			IdxValue <= {15'b0, encoded_frame_reg[wire_cnt_num_bit_minus_one]};
			Addr_RF <= 12'd3840 + {5'b0, wire_cnt_num_bit_minus_one};
			WrtEn_RF <= 0;
			FinishIdxEncode	<= 0;
		end
		S_Finish:begin
			IdxValue <= 0;
			Addr_RF <= 0;
			WrtEn_RF <= 0;
			FinishIdxEncode	<= 1;
		end
		default: begin 
			IdxValue <= 0;
			Addr_RF <= 0;
			WrtEn_RF <= 0;
			FinishIdxEncode	<= 0;
		end
		endcase
	end
end
`endif
endmodule

/*
	case(state)
	S_Idle:begin

	end
	S_Fetch_Frame_RdEn:begin

	end
	S_Fetch_Frame_LoadData:begin

	end
	S_Encoding_Judgebit:begin

	end
	S_Encoding_WriteIndex1:begin

	end
	S_Encoding_WriteIndex2:begin

	end
	S_Encoding_WriteIndex3:begin

	end
	S_Finish:begin

	end
	default: begin 

	end
	endcase
*/

