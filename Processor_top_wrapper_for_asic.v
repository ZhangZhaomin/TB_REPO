`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/07/21 20:11:24
// Design Name: 
// Module Name: Processor_top_wrapper_for_asic
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Processor_top_wrapper_for_asic(
    input  clk,
    input rst_n,
    input en,
    input fifo_empty,
    // input [15:0] fifo_read;
    input [3:0] config_set,
    // input [3:0] drop_out,
    input [95:0] dout,
    
    output reg rd_en,
    output reg finish,
    output reg [7:0] data2tx,
    output reg totx_ready,
    output reg [11:0]  beat,
    output reg initial_ok,

    output reg [2:0] OpCode,
    output reg nn_en,
    output  nn_ram_we,
    output  [11:0] nn_ram_addr,
    output  [15:0] nn_ram_din,
    output reg [7:0] Monitor_opcode,
    input nn_finish,
    input [4:0] result,
    input [15:0] Check_out
  
    );



parameter T = 5;

localparam [7:0] idel = 8'b0000_0001,
              fetch = 8'b0000_0010,

              //initial
              init_pre_pre = 8'b1000_0000,
              init_pre = 8'b0000_0011,
              init = 8'b1000_0011,
              init_post = 8'b1000_0100,   //循环3684次

              //ANN inference
              load_drop_pre = 8'b0000_0100,     //ANN learning 也要调用drop、input、label
              load_drop = 8'b0000_0101,
              load_drop_post = 8'b0000_0110,    //循环3个dropout
              //load_input_pre_pre = 8'b1000_0111,
              load_input_pre  = 8'b0000_0111,
              load_input = 8'b0000_1000,
              load_input_post = 8'b0000_1001,   //循环96个input
              load_label_pre = 8'b0000_1010,
              load_label = 8'b0000_1011,
              ann_infer_pre = 8'b0000_1100,   //拉低写使能
              ann_infer = 8'b0000_1101,
              ann_infer_post = 8'b0000_1110,
              ann_predict = 8'b0000_1111,
              ann_predict_post = 8'b0001_0000,
              compare = 8'b0001_0001,             //AI、SI共用
              post_process_pre = 8'b0001_0010,    //AI、SI共用 回送uart,AI与SI共用，备用几个状态
              post_process = 8'b0001_0011,
              post_process_post = 8'b0001_0100,

              //ANN learning
              load_lr_rate_pre = 8'b0001_0101,
              load_lr_rate = 8'b0001_0110,
              load_lr_rate_post = 8'b0001_0111,  //下一个加载dropout
              ale_load_drop_pre = 8'b0001_1100,
              ale_load_drop = 8'b0001_1101,
              ale_load_drop_post = 8'b0001_1110,
              ann_transfer_pre = 8'b0001_1000,
              ann_transfer = 8'b0001_1001,
              ann_transfer_post = 8'b0001_1010,   //下一状态idel

              //SNN inference
              load_th_pre = 8'b0001_1011,
              load_th = 8'b0001_1100,
              load_th_post = 8'b0001_1101,  //循环3个threshold，之后切换到load_input_pre，灌label
              clean_neuron_pre = 8'b0001_1110,
              clean_neuron = 8'b0001_1111,
              clean_neuron_post = 8'b0010_0000,   //循环200+次
              raw_spike2index_pre = 8'b0010_0001,     //夏子寒，从FIFO取原始96bit脉冲
              raw_spike2index = 8'b0010_0010,
              raw_spike2index_post = 8'b0010_0011,
              //预留多个状态
              //.......
              //灌完数据，开始推理
              snn_infer_pre = 8'b0010_1000,
              snn_infer = 8'b0010_1001,
              snn_infer_post = 8'b0010_1010,
              add_volt = 8'b0010_1011,
              add_volt_post = 8'b0010_1100,    //if count < T , next <= get_raw_spike  else next <= snn_predict
              snn_predict = 8'b0010_1101,
              snn_predict_post = 8'b0010_1110;  //下一个状态compare


reg [7:0] curr_state;
reg [7:0] next_state;

reg [3:0] drop_out_reg;
reg [3:0] config_set_reg;

always @(posedge clk or negedge rst_n) begin
    if(~rst_n)  begin
        curr_state <= idel;
    end  else begin
        curr_state <= next_state;
    end
end

// reg en_reg;
reg [11:0] count;
reg [11:0] t_count;

wire index_finish;

always @(*) begin
    next_state = curr_state;
    case(curr_state)
        idel:  begin                          //清零count
            if((~fifo_empty) ||(en & (config_set == 4'b1000)))  next_state = fetch;
        end
        fetch:  begin
                case ({config_set})
                    4'b1000:  next_state =  init_pre_pre;   //initial
                    4'b01xx:  next_state =  load_drop_pre;  //ann inference (0100), or learning(0110,0111,0101)
                    // 4'b0110:  next_state =  load_lr_rate_pre;  //ann learning
                    4'b0001:  next_state =  load_th_pre;  //snn inference
                    default:  next_state = idel;
                endcase 
        end

        //initial 
        init_pre_pre:  begin   //初始化权值
            next_state = init_pre;
        end
        init_pre:  begin
            next_state = init;
        end
        init:  begin
            next_state = init_post;
        end
        init_post:  begin
            if(count < 12'd3664)  next_state = init_pre_pre;
            else  next_state = idel;
        end

        //ann inference
        load_drop_pre:  begin
            next_state = load_drop;
        end
        load_drop:  begin
            next_state = load_drop_post;
        end
        load_drop_post:  begin
            if(count < 12'd3)  next_state = load_drop_pre;
            else  next_state = load_input_pre;
        end
        load_input_pre:  begin
            next_state = load_input;
        end
        load_input:  begin
            next_state = load_input_post;
        end
        load_input_post:  begin
            if(count < 12'd96)  next_state = load_input_pre;     
            else  next_state = load_label_pre;
        end
        load_label_pre:  begin
            next_state = load_label;
        end
        load_label:  begin
            case(config_set_reg[2:0])  
                3'b100:  next_state =  ann_infer_pre;
                3'b010:  next_state =  ann_infer_pre;
                3'b001:  next_state =  snn_infer_pre;
            endcase   
        end
        ann_infer_pre:  begin
            if(count == 12'd3)  next_state = ann_infer;
        end
        ann_infer:  begin
            if(nn_finish)  next_state = ann_infer_post;
        end
        ann_infer_post:  begin
            if(config_set_reg == 4'b0100)  next_state = ann_predict;
            else  next_state = load_lr_rate_pre; //                     transfer to ann learning mode
        end
        ann_predict:  begin
            if (count == 12'd3)  next_state = ann_predict_post;
        end
        ann_predict_post:  begin
            if(nn_finish)  next_state = compare;
        end
        //AI、SI共用
        compare:  begin
            next_state = post_process_pre;
        end
        post_process_pre:  begin
            next_state = post_process;
        end
        post_process:  begin
            next_state = post_process_post;
        end
        post_process_post:  begin
            next_state = idel;
        end

        //ann learning================================================================================================
        load_lr_rate_pre:  begin
            next_state = load_lr_rate;
        end
        load_lr_rate:  begin
            next_state = load_lr_rate_post;
        end
        load_lr_rate_post:  begin
            next_state = ale_load_drop_pre;
        end
        ann_transfer_pre: begin
            if (count == 3) next_state = ann_transfer;
            else next_state = ann_transfer_pre;
        end
        ann_transfer:begin
            if(nn_finish)  next_state = ann_transfer_post;
        end
        ann_transfer_post:begin
            next_state = idel;
        end
        //============================================================================================================
        //snn inference
        load_th_pre:  begin
            next_state = load_th;
        end
        load_th:  begin
            next_state = load_th_post;
        end
        load_th_post:  begin
            if(count < 12'd3)  next_state = load_th_pre;
            else  next_state = clean_neuron_pre;
        end
        clean_neuron_pre:  begin
            next_state = clean_neuron;
        end
        clean_neuron:  begin
            next_state = clean_neuron_post;
        end
        clean_neuron_post:  begin
            if(count < 12'd206)  next_state = clean_neuron_pre;
            else  next_state = raw_spike2index_pre;   //for 夏子寒 to do 
        end

        raw_spike2index_pre:  begin
            next_state = raw_spike2index;
        end
        raw_spike2index:  begin
            if(index_finish)  next_state = raw_spike2index_post;
        end
        raw_spike2index_post:  begin
            next_state = snn_infer_pre;
        end
        snn_infer_pre:  begin
            if(count == 12'd3)  next_state = snn_infer;
        end
        snn_infer:  begin
            if(nn_finish)  next_state = snn_infer_post;
        end
        snn_infer_post:  begin
            if (count == 12'd3)  next_state = add_volt;
        end
        add_volt:  begin
            next_state = add_volt_post;
        end
        add_volt_post:  begin
            if(nn_finish & (t_count >= T))  next_state = snn_predict;
            else if (nn_finish & (t_count < T))  next_state = raw_spike2index_pre;
        end
        snn_predict:  begin
            if (count == 12'd3)  next_state = snn_predict_post;
        end
        snn_predict_post:  begin
            if(nn_finish)  next_state = compare;
        end
        default:  begin
            next_state = idel;
        end 
    endcase
end


wire [15:0] spo;
reg [15:0] spo_reg;
// reg nn_en;
// reg [3:0] Monitor_opcode;
// reg [2:0] OpCode;
wire [11:0] a;
reg [11:0] RAM_ADDR;
reg RAM_WE;
reg [15:0] RAM_DIN;
reg [95:0] input_reg;

reg StartIdxEncode;
reg [95:0] EncodedFrame;


assign a = (config_set[3] == 1'b1)?count:12'd0;

always @(posedge clk or negedge rst_n) begin
    if(~rst_n)  begin
        count <= 12'd0;
        drop_out_reg <= 4'd0;
        config_set_reg <= 4'd0;
        spo_reg <= 16'd0;
        Monitor_opcode <= 4'd0;
        nn_en <= 1'b0;
        OpCode <= 3'd0;
        RAM_ADDR <= 12'd0;
        RAM_WE <= 1'b0;
        RAM_DIN <= 16'd0;
        input_reg <= 96'd0;
        data2tx <= 8'd0;
        totx_ready <= 1'b0;
        finish <= 1'b0;
        t_count <= 12'd0;
        StartIdxEncode <= 1'b0;
        EncodedFrame <= 96'd0;
        initial_ok <= 1'b0;
        beat <= 12'd0;
        // nn_finish <= 1'b0;
        // result <= 5'd0;
        // Check_out <= 16'd0;
        // en_reg <= 1'b0;
    end  else begin
        // en_reg <= en;
         case (curr_state)  
            idel:  begin
                count <= 12'd0;
                RAM_WE  = 1'b0;
                finish <= 1'b1;
                nn_en <= 1'b0;
                totx_ready <= 1'b0;
            end
            fetch:  begin
                // drop_out_reg <= drop_out;
                config_set_reg <= config_set;
                nn_en  <= 1'b1;
                Monitor_opcode[3:0] <= 4'h07;
                OpCode  <= 3'b111;
                finish <= 1'b0;
            end

            init_pre_pre:  begin
                //a <= count;
                spo_reg <= spo;    
            end
            init_pre:  begin
                count <= count + 1'b1;
                RAM_ADDR <= count;
                RAM_DIN <= spo_reg;
            end
            init:  begin
                RAM_WE <= 1'b1;
            end
            init_post:  begin
                RAM_WE <= 1'b0;
                if(count == 12'd3664)  initial_ok <= 1'b1;
            end

            //ann inference
            load_drop_pre:  begin

                count <= count +1'b1;
                if (count == 12'd0)  rd_en <= 1'b1;    
                case (count) 
                    12'd0:  begin
                        RAM_ADDR  <= 12'd3666;
                        RAM_DIN <=16'b0000_0000_0000_0000;
                    end         
                    12'd1:  begin
                        RAM_ADDR  <= 12'd3667;
                        RAM_DIN <= (config_set_reg[1:0] != 2'b00)?16'b0000_0000_0000_0101:16'd0;
                    end
                    12'd2:  begin
                        RAM_ADDR  <= 12'd3668;
                        RAM_DIN <= 16'd0;
                    end
                    default:  begin
                        RAM_ADDR  <= 12'd3800;//holder
                        RAM_DIN <= 16'd0;
                    end
                endcase
            end
            load_drop:  begin
                RAM_WE <= 1'b1;
                rd_en <= 1'b0; 
                input_reg <= dout;   //提前加载输入
            end
            load_drop_post:  begin
                RAM_WE <= 1'b0;
                if(count >= 12'd3)  count <= 12'd0;
            end
            load_input_pre:  begin
                count <= count +1'b1;
                RAM_ADDR  <= 12'd3935 - count;
                RAM_DIN <= input_reg[count]?16'b0000010000000000:16'd0;  //1bit to 16bit
            end
            load_input:  begin
                RAM_WE <= 1'b1;
            end
            load_input_post:  begin
                RAM_WE <= 1'b0;
                if(count >= 12'd96)  count <= 12'd0;
            end
            load_label_pre:  begin
                count <= 12'd0;
                //RAM_ADDR  <= 12'd4042;
            end
            ann_infer_pre:  begin
                nn_en  <= 1'b1;
                OpCode  <= 3'b100;
                count <= count +1'b1;
            end
            ann_infer:  begin
                nn_en  <= 1'b0;
                count <= 12'd0;
            end
            ann_infer_post:  begin
                if(config_set_reg == 4'b0100)  begin
                    nn_en  <= 1'b1;
                    OpCode  <= 3'b110;//predict
                end
                else if (config_set_reg[1:0] != 2'b00)  begin
                    nn_en  <= 1'b1;
                    OpCode  <= 3'b111;  // write ram
                end
            end

            //ann predict
            ann_predict:  begin
                nn_en  <= 1'b0;
                count <= count +1'b1;
            end
            ann_predict_post:  begin
                //holder
                // totx_ready <= 1'b0;
            end
            compare:  begin
                //holder
                beat <= beat + 1'b1;
                data2tx <= result;
                totx_ready <= 1'b1;
            end
            post_process_pre:  begin
                // data2tx <= result;
                totx_ready <= 1'b0;
            end
            post_process:  begin
                // totx_ready <= 1'b1;
            end
            post_process_post:  begin
                // totx_ready <= 1'b0;
            end

            //ann learning ===========================================================================================================================
            load_lr_rate_pre:  begin
                RAM_ADDR  <= 12'd3665;
                RAM_DIN <=  16'b0000_1000_0000_0000;
                RAM_WE <= 1'b0;   
                nn_en  <= 1'b0;
                OpCode  <= 3'b111;  // write ram 
            end
            load_lr_rate:  begin
                RAM_ADDR  <= 12'd3665;
                RAM_DIN <=  16'b0000_1000_0000_0000;
                RAM_WE <= 1'b1;    
                nn_en  <= 1'b0;
                OpCode  <= 3'b111;  // write ram        
            end
            load_lr_rate_post:  begin
                RAM_ADDR  <= 12'd3665;
                RAM_DIN <=  16'b0000_1000_0000_0000;
                RAM_WE <= 1'b0;  
                nn_en  <= 1'b0;
                OpCode  <= 3'b111;  // write ram           
            end
            ann_transfer_pre: begin
                nn_en  <= 1'b1;
                OpCode  <= 3'b101;  // 
            end
            ann_transfer:begin
                nn_en  <= 1'b0;
                OpCode  <= 3'b101;  
            end
            ann_transfer_post:begin
                nn_en  <= 1'b0;
                OpCode  <= 3'b101;  
            end
            //========================================================================================================================================

            //snn inference
            load_th_pre: begin
                count <= count +1'b1;
                if (count == 12'd0)  rd_en <= 1'b1;    
                case (count) 
                    12'd0:  begin
                        RAM_ADDR  <= 12'd3669;
                        RAM_DIN <= 16'b0010101110011011;
                    end         
                    12'd1:  begin
                        RAM_ADDR  <= 12'd3670;
                        RAM_DIN <=  16'b0001000101010110;
                    end
                    12'd2:  begin
                        RAM_ADDR  <= 12'd3671;
                        RAM_DIN <= 16'b0001000001100101;
                    end
                    default:  begin
                        RAM_ADDR  <= 12'd3800;//holder
                        RAM_DIN <= 16'd0;
                    end
                endcase
            end
            load_th:  begin
                RAM_WE <= 1'b1;
                rd_en <= 1'b0; 
                input_reg <= dout;
            end
            load_th_post:  begin
                RAM_WE <= 1'b0;
                if(count >= 12'd3)  count <= 12'd0;
            end
            clean_neuron_pre:  begin
                count <= count + 1'b1;
                RAM_ADDR  <= 12'd3840 + count;
                RAM_DIN <= 16'd0;
                if (count == 12'd0)  t_count <= 12'd0;
            end
            clean_neuron:  begin
                RAM_WE <= 1'b1;
            end
            clean_neuron_post:  begin
                RAM_WE <= 1'b0;
                if(count >= 12'd206)  count <= 12'd0;
            end
            raw_spike2index_pre:  begin
                StartIdxEncode <= 1'b1;
                EncodedFrame <= input_reg;
                t_count <= t_count + 1'b1;
            end
            raw_spike2index: begin
                StartIdxEncode <= 1'b0;
            end
            raw_spike2index_post:  begin
                count <= 12'd0;
            end
            snn_infer_pre:  begin
                nn_en  <= 1'b1;
                OpCode  <= 3'b000;
                count <= count +1'b1;
            end
            snn_infer:  begin
                nn_en  <= 1'b0;
                count <= 12'd0;
            end
            snn_infer_post:  begin
                OpCode <= 3'b001;
                nn_en  = 1'b1;
                count <= count + 1'b1;
            end
            add_volt:  begin
                nn_en  = 1'b0;
            end
            add_volt_post:  begin
                count <= 12'd0;
            end
            snn_predict:  begin
                OpCode <= 3'b010;
                nn_en  <= 1'b1;
                count <= count +1'b1;
            end
            snn_predict_post:  begin
                nn_en  <= 1'b0;
                // totx_ready <= 1'b0;
            end

         default:  begin
             count <= 12'd0;
         end 

         endcase
    end


end

wire [15:0] IdxValue;
wire [11:0] Addr_RF;
wire WrtEn_RF;

// wire nn_ram_we;
// wire [11:0] nn_ram_addr;
// wire [15:0] nn_ram_din;


assign nn_ram_we = (curr_state == raw_spike2index)?WrtEn_RF: RAM_WE;
assign nn_ram_addr = (curr_state == raw_spike2index)?Addr_RF: RAM_ADDR;
assign nn_ram_din = (curr_state == raw_spike2index)?IdxValue: RAM_DIN;

IndexEncoder u_IndexEncoder (
		.clk(clk), 
        .rst_n(rst_n),
		.StartIdxEncode(StartIdxEncode),
		.EncodedFrame(EncodedFrame),

		.IdxValue(IdxValue),
		.Addr_RF(Addr_RF), // 0,...
		.WrtEn_RF(WrtEn_RF), // 3 clks, mimic the latch timing constrains
		.FinishIdxEncode(index_finish)
	);

initial_ram u_init_ram (.a(a),.spo(spo));



endmodule
